#pragma once
#include "AbstractPlotTransformer.h"
class SquareTransformer :
	public AbstractPlotTransformer
{
public:
	SquareTransformer();
	Point transform(Point const) override;
	~SquareTransformer();
};

