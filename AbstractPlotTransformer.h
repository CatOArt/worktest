#pragma once
#include "Point.h"

#include <iostream>//TODO remove1
class AbstractPlotTransformer
{
public:
	AbstractPlotTransformer() { std::cout << "Constr Transformer"; };
	virtual Point transform(Point const) = 0;
	virtual ~AbstractPlotTransformer() { std::cout << "Destr Transformer"; };
};

