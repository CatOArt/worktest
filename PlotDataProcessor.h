#pragma once
#include "AbstractPlotTransformer.h"
#include <memory>

class PlotDataProcessor
{
public:
	PlotDataProcessor();
	~PlotDataProcessor();
	void setTransformer(std::unique_ptr<AbstractPlotTransformer>);
private:
	std::unique_ptr<AbstractPlotTransformer> transformer;
};

