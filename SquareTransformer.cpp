#include "SquareTransformer.h"



SquareTransformer::SquareTransformer()
{
}

Point SquareTransformer::transform(Point const raw)
{
	return Point(raw.x,raw.y*raw.y);
}


SquareTransformer::~SquareTransformer()
{
}
